@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Ubah Data Peminjaman Buku</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li>
                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li>
                <a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <!-- start form for validation -->
        <form action="{{ route('borrow.update', $borrow->id) }}" method="post">
            @csrf
            @method('put')
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="siswa"> Siswa</label>
                            <select class="form-control" id="siswa" name="siswa">
                                <option>chose...</option>
                                @foreach ($siswa as $item)
                                <option value="{{ $item->id }}" {{ $borrow->siswa_id == 
                                $item->id ? 'selected' : ''}}> {{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="book">Buku</label>
                            <select class="form-control" id="book" name="book">
                                <option>chose...</option>
                                @foreach ($books as $item)
                                <option value="{{ $item->id }}" {{ $borrow->book_id == 
                                $item->id ? 'selected' : ''}}> {{ $item->title }}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="start">Tanggal Pinjam</label>
                        <input type="date" class="form-control" id="start" 
                        name="start" value="{{ $borrow->start }}" >
                    </div>
                    <div class="form-group col-md-6">
                        <label for="return">Tanggal Kembali <small id='kembali'></small></label>
                        <input type="date" class="form-control" id="return" 
                        name="return"  >
                    </div>
                    <div class="from-row">
                        <div class="col-md-12">
                            <label class="mr-4">Status</label><br>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" name="status" id="status1" value="Dipinjam" 
                                class="custom-control-input" 
                                {{ $borrow->status == 'Dipinjam' ? 'checked' : '' }}>
                                <label class="custom-control-label" for="status1">Dipinjam</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                 <input type="radio" name="status" id="status2" value="Dikembalikan"
                                  class="custom-control-input" 
                                {{ $borrow->status == 'Dikembalikan' ? 'checked' : '' }}>
                                <label class="custom-control-label" for="status2">Dikembalikan</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12"><br>
                        <label for="denda">Denda</label>
                        <input type="text" class="form-control" id="denda" 
                        name="denda" readonly>
                    </div>
                </div>
                <button type="submit" class="btn btn-danger">Simpan</button>
        </form>
    <!-- end form for validations -->
    </div>
</div>
@endsection

@push('script')
<script>
    $('#start, #return').change(()=>{
        var pinjam = $('#start').val()
        var kembali = $('#return').val()
        //cek jika input pinjam dan kembali tidak kosong
        if(Date.parse(pinjam) && Date.parse(kembali) )

        {
            pinjam = new Date(pinjam)
            kembali = new Date(kembali)

            // membuat tanggal deadline
            var deadline = pinjam.setDate(pinjam.getDate() + 7)

            //menghitung selisih hari dan denda
            var selisih = (kembali - deadline) / (1000 * 3600 * 24)
            var denda = selisih > 0 ? (selisih*500) : 0;

            $('#denda').val(denda)

            //membuat fungsi data tanggal deadline untuk ditampilkan 
            var convertDeadline = new Date (deadline)
            var formattedDate = ('0' + convertDeadline.getDate()).slice(-2);
            var formattedMonth = ('0' + (convertDeadline.getMonth() + 1)).slice(-2);
            var formattedYear = convertDeadline.getFullYear.toString().substr(2,2);
            var dateString = formattedMonth + '/' + formattedDate + '/' + formattedYear;

            //menampilkan deadline
            $('#kembali').text( '(Deadline : '+dateString+')' )


        }else {
            $('#kembali').text('(Deadline : )')
            $('#denda').val(0)
        }
    
    })
</script>
@endpush
