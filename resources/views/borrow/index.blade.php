@extends('layouts.template')

@section('content')
<div class="col-md-12 col-sm-12">
    <div class="row">
      <div class="x_panel">
        <div class="x_title">
          <h2>Data Peminjaman Siswa</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li>
              <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li>
              <a href="{{ route('borrow.create') }}">
                <button type="button" class="btn btn-primary btn-sm">Tambah</button>
              </a>
            </li>
            <li>
              <a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <table class="table table-striped">
              <thead>
                <tr style="text-align: center;">
                    <th scope="col">No</th>
                    <th scope="col">Nama Siswa</th>
                    <th scope="col">Judul Buku</th>
                    <th scope="col">Tanggal Peminjaman Buku</th>
                    <th scope="col">Status Peminjaman</th>
                    <th scope="col">Menu</th>

                </tr>
              </thead>
              <tbody>
                @foreach ($borrows as $i => $borrow)
                <tr style="text-align: center;">
                  <td>{{ $i+1 }}</td>
                  <td>{{ $borrow->borosRef->name}}</td>
                  <td>{{ $borrow->boroRef->title }}</td>
                  <td>{{ $borrow->start }}</td>
                  <td>{{ $borrow->status }}</td>
                  <td>
                    <form action="{{ route('borrow.destroy',$borrow->id) }}" method="post">
                    <a href="{{ route('borrow.edit',$borrow->id) }}">
                      <button type="button" class="btn btn-success">Ubah</button>
                    </a>
                    @csrf 
                    @method('delete')
                      <button type="submit" class="btn btn-danger" 
                      onclick="return confirm('yakin dihpus hehehe?' )">Hapus</button>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
