@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Tambah Data Peminjaman Buku</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li>
                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li>
                <a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <!-- start form for validation -->
        <form action="{{ route('borrow.store') }}" method="post">
            @csrf
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="siswa"> Siswa</label>
                            <select class="form-control" id="siswa" name="siswa">
                                <option>chose...</option>
                                @foreach ($siswa as $borrow)
                                <option value="{{ $borrow->id }}">{{ $borrow->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="book">Buku</label>
                            <select class="form-control" id="book" name="book">
                                <option>chose...</option>
                                @foreach ($books as $borrow)
                                <option value="{{ $borrow->id }}">{{ $borrow->title }}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="start">Tanggal Pinjam</label>
                        <input type="date" class="form-control" id="start" 
                        name="start" placeholder="Masukkan Tanggal Pinjam " >
                    </div>
                    </div>
                <button type="submit" class="btn btn-danger">Simpan</button>
        </form>
    <!-- end form for validations -->
    </div>
</div>
@endsection
