<?php

namespace App\Http\Controllers;

use App\Borrow;
use App\Siswa;
use App\Book;
use Illuminate\Http\Request;

class BorrowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borrows = Borrow::with('borosRef','boroRef')->get();
        return view('borrow.index', compact('borrows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $borrows = Borrow::all();
        $siswa = Siswa::all();
        $books = Book::all();
        return view('borrow.create', compact('siswa', 'books'));
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $borrow = new Borrow;
       $borrow->book_id = $request->book;
       $borrow->siswa_id = $request->siswa;
       $borrow->start = $request->start;
       $borrow->status = 'Dipinjam';
       $borrow->save();
       return redirect()->route('borrow.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Borrow  $borrow
     * @return \Illuminate\Http\Response
     */
    public function show(Borrow $borrow)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Borrow  $borrow
     * @return \Illuminate\Http\Response
     */
    public function edit(Borrow $borrow)
    {
        $siswa = Siswa::all();
        $books = Book::all();
        return view('borrow.edit', compact('siswa', 'books', 'borrow'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Borrow  $borrow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Borrow $borrow)
    {
        $borrow->book_id = $request->book;
        $borrow->siswa_id = $request->siswa;
        $borrow->start = $request->start;
        $borrow->return = $request->return;
        $borrow->denda = $request->denda;
        $borrow->status = $request->status;
        $borrow->update();
        return redirect()->route('borrow.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Borrow  $borrow
     * @return \Illuminate\Http\Response
     */
    public function destroy(Borrow $borrow)
    {
        $borrow->delete();
        return redirect()->route('borrow.index');
    }
}
